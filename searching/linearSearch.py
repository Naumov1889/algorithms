def linearSearch(myList, target):
    for i in range(0, len(myList)):
        if target == myList[i]:
            return i
    return -1

theList = [3, 2, 4, 1]
print(linearSearch(theList, 4))

'''
We use linearSearch to search a collection of elements for one specific element.
What a linear search does is it simply looks at each number in sequence and evaluates if it's what you're looking for.
Best practice: If element is not found returns a value of negative one(-1).
As soon as we get return program stops. So If the element we're looking for is 4. Program check 3, then 2, then it sees myList[i] matches 4, program returns index of 4 and stops.
'''