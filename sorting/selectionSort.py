def selectionSort(myList):
    for i in range(0, len(myList) - 1):
        minIndex = i

        for k in range(i+1, len(myList)):
            if myList[minIndex] > myList[k]:
                minIndex = k

        if minIndex != i:
            myList[i], myList[minIndex] = myList[minIndex], myList[i]

    return myList

theList = [3, 2, 4, 1]
print(selectionSort(theList))

# ==EXPLANATION==BEGIN
'''
Selection sort is not more efficient than bubbble sort.
'''

'''
We have sorted and unsorted parts of the list.

We are trying to define the smallest number of unsorted part and append it into the last position of sorted part. 
When i say append i mean: the first number of unsorder part is going to swap with the smallest number of unsorted part as well. If the first number = the smallest number then no swap. And then the sorted list increases on 1 element and unsorted part decreases on 1 element.

"len(myList) - 1" in "for i in range(0, len(myList) - 1)" — the last element will be sorted by default. He is going to be the only one in unsorted part. When everyone but one is sorted we don't need to sort the one.
And what's more. Look here. 
if we have no -1 there then: once i = 3. Now look further:
"for k in range(i+1, len(myList))". So 3+1=4.  range(4, 4) looks stupid because we don't even have anything under index 4. And we don't even have a number  between (4, 4). It's not even 4.

"i+1" in "for k in range(i+1, len(myList))" — divide sorted and unsorted parts of the list. So new round of nested loop starts with unsorted part.

"if minIndex != i" — we don't want to swap number on itself. In other words, if the min item is already in the leftmost position then there's no need to do the swap.﻿


'''

# for i in range(0, 4 - 1) = for i in range(0, 3)

# minIndex = 0
# for k in range(0+1, 4) = for k in range(1, 4)
'''
minIndex = 0
k = 1
3 > 2 → minIndex = 1
------------------
minIndex = 1
k = 2
2 < 4 —/→
------------------
minIndex = 1
k = 3
2 > 1 → minIndex = 3 
------------------
------------------
0 != 3 → [1, 2, 4, 3]
'''

# minIndex = 1
# for k in range(2, 4)
'''
minIndex = 1
k = 2
2 < 4 —/→
------------------
minIndex = 1
k = 3
2 < 3 —/→
------------------
------------------
1 = 1 —/→   
'''

#minIndex = 2
# for k in range(3, 4)
'''
minIndex = 2
k = 3
4 > 3 → minIndex = 4
------------------
------------------
2 != 4 → [1, 2, 3, 4]
'''
# ==EXPLANATION==END


'''
==RESOURSEs==BEGIN
https://www.youtube.com/watch?v=mI3KgJy_d7Y&index=2&list=PLj8W7XIvO93rJHSYzkk7CgfiLQRUEC2Sq
https://cdn-images-1.medium.com/max/750/1*S-wdMkkaX3Gr4bQrbqu_1Q.jpeg
==RESOURSEs==END
'''