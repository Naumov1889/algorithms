def bubbleSort(myList):
    for i in range(0, len(myList) - 1):
        for k in range(0, len(myList) - 1 - i):
            if myList[k] > myList[k+1]:
                myList[k], myList[k+1] = myList[k+1], myList[k]
    return myList

theList = [3, 2, 4, 1]
print(bubbleSort(theList))

'''
Following works too but it's not efficiently. You can try print myList every round(for k loop) to make sure(do the same with code above and compare).
def bubbleSort(myList):
    for i in range(0, len(myList)):
        for k in range(0, len(myList) - 1):
            if myList[k] > myList[k+1]:
                myList[k], myList[k+1] = myList[k+1], myList[k]
    return myList

theList = [3, 2, 4, 1]
print(bubbleSort(theList))
'''

# ==EXPLANATION==BEGIN
# author(of video) explanation of -1:
# "len(myList) - 1" — If we have a 4 item list as in this example, the indices are 0, 1, 2, 3. And the comparisons are 0 to 1, 1 to 2, and 2 to 3. So we only want three loop iterations for a four item list. Our loop goes from 0 to n-1, which means it will iterate for i=0, i=1 and i=2. We have no pair for comparison 3 to 4(we have no number under 4 index).

# my understanding:
# "len(myList) - 1 - i)" — The first round(for i loop) and the last number is the greatest, the second round and the last but one is the greatest after the last..... and so on. So It doesn't make any sense to go through them when we itterate.
# "len(myList) - 1" — considering explanation of "len(myList) - 1 - i)" when we have the last but two(4-the last, 3-the last but one, 2-the last but two) we don't need to make sure that the greatest but four is the smallest number.


# for i in range(0, 4 - 1) = for in in range(0, 3)

# i = 0
# for k in range(0, 4 - 1 - 0) = for k in range(0, 3)
'''
k = 0
3 > 2 → 2341
------------------
k = 1
3 < 4 —/→ 
------------------
k = 2
4 > 1 → 2314
'''

# i = 1
# for k in range(0, 4 - 1 - 1) = for k in range(0, 2)
'''
k = 0
2 < 3 —/→
------------------
k = 1
3 > 1 → 2134
------------------
'''

# i = 2
# for k in range(0, 4 - 1 - 2) = for k in range(0, 1)
'''
k = 0
2 > 1 → 1234
'''
# ==EXPLANATION==END

'''
==RESOURSEs==BEGIN
https://www.youtube.com/watch?v=YHm_4bVOe1s
https://codingcompiler.com/wp-content/uploads/2017/10/bubble-sort-in-c.png
==RESOURSEs==END
'''